## Setup

This repository is a basic GUI that allows the user to test the capabilities of the library **ekuore-pro-for-telemedicine-engine-java**. 
You need to include to your project the `'ekuore-pro-for-telemedicine-engine-java-release.jar'` and satisfy its dependency, which is `'protobuf-java-3.11.3'`.
This repository does not contain any project configuration; compile it under Gradle, ANT or Maven at your own discretion.

## Usage

Once compiled and running, the first step is to configure the connection with the eKP4T device. Check the user manual of the library to better understand how
to use it.

Once configured, input the connection parameters and click "Start streaming".