package com.ekuore.ekuoreprofortelemedicine;

import com.ekuore.ekuoreprofortelemedicine.enums.FilterMode;
import com.ekuore.ekuoreprofortelemedicine.enums.SecurityType;

import java.io.IOException;
import java.io.PrintStream;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Tester {
    private int packetReceivedCounter = 0;

    private JTextField textFieldConfigureControlPort;
    private JTextField textFieldConfigureStreamingPort;
    private JRadioButton radioButtonOpen;
    private JRadioButton radioButtonWep;
    private JRadioButton radioButtonWpa;
    private JTextField textFieldPassword;
    private JTextField textFieldSsid;
    private JButton buttonSendConfiguration;
    private JTextField textFieldIp;
    private JButton buttonConnect;
    private JButton buttonDisconnect;
    private JButton buttonGetSerialNumber;
    private JButton buttonGetHardwareVersion;
    private JButton buttonGetDSPVersion;
    private JButton buttonGetBatteryLevel;
    private JButton buttonGetFirmwareVersion;
    private JButton buttonGetVolumeLevel;
    private JButton buttonGetLedStatus;
    private JButton buttonGetFilter;
    private JTextField textFieldVolumeLevel;
    private JButton buttonSetVolumeLevel;
    private JRadioButton radioButtonOn;
    private JRadioButton radioButtonOff;
    private JRadioButton radioButtonHeart;
    private JRadioButton radioButtonLung;
    private JRadioButton radioButtonExtended;
    private JButton buttonSetLed;
    private JButton buttonSetFilter;
    private JTextArea textAreaLogs;
    private JButton buttonClearLogs;
    private JPanel mainPanel;
    private JTextField textFieldControlPort;
    private JTextField textFieldStreamingPort;

    private Engine engine = Engine.getInstance();

    public Tester() {
        PrintStream printStream = new PrintStream(new TextAreaOutputStream(textAreaLogs));
        System.setOut(printStream);
        System.setErr(printStream);

        buttonSendConfiguration.addActionListener(e -> {
            try {
                if (radioButtonOpen.isSelected()) {
                    engine.configureConnection(SecurityType.OPEN,
                            textFieldSsid.getText(),
                            textFieldPassword.getText(),
                            Integer.parseInt(textFieldConfigureControlPort.getText()),
                            Integer.parseInt(textFieldConfigureStreamingPort.getText()));
                }

                if (radioButtonWep.isSelected()) {
                    engine.configureConnection(SecurityType.WEP,
                            textFieldSsid.getText(),
                            textFieldPassword.getText(),
                            Integer.parseInt(textFieldConfigureControlPort.getText()),
                            Integer.parseInt(textFieldConfigureStreamingPort.getText()));
                }

                if (radioButtonWpa.isSelected()) {
                    engine.configureConnection(SecurityType.WPA_WPA2,
                            textFieldSsid.getText(),
                            textFieldPassword.getText(),
                            Integer.parseInt(textFieldConfigureControlPort.getText()),
                            Integer.parseInt(textFieldConfigureStreamingPort.getText()));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonConnect.addActionListener(e -> {
            try {
                engine.startStreaming(packet -> {
                            packetReceivedCounter = packetReceivedCounter + 1;
                            if (packetReceivedCounter % 1000 == 0) {
                                System.out.println(packetReceivedCounter + " audio packets received");
                            }
                        },
                        textFieldIp.getText(),
                        Integer.parseInt(textFieldControlPort.getText()),
                        Integer.parseInt(textFieldStreamingPort.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonDisconnect.addActionListener(e -> {
            try {
                engine.disconnect();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetSerialNumber.addActionListener(e -> {
            try {
                engine.getSerialNumber(sn -> System.out.println("Serial number: " + sn));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetHardwareVersion.addActionListener(e -> {
            try {
                engine.getHardwareVersion(hardwareVersion -> System.out.println("Hardware version: " + hardwareVersion));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetDSPVersion.addActionListener(e -> {
            try {
                engine.getDspVersion(parsedDspVersion -> System.out.println("DSP version: " + parsedDspVersion));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetBatteryLevel.addActionListener(e -> {
            try {
                engine.getBatteryLevel(batteryLevel -> System.out.println("Battery level: " + batteryLevel));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetFirmwareVersion.addActionListener(e -> {
            try {
                engine.getFirmwareVersion(firmwareVersion -> System.out.println("Firmware version: " + firmwareVersion));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetFilter.addActionListener(e -> {
            try {
                engine.getFilter(newFilterStatus -> System.out.println("Filter status: " + newFilterStatus));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetVolumeLevel.addActionListener(e -> {
            try {
                engine.getVolumeLevel(volumeLevel -> System.out.println("Volume level: " + volumeLevel));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonGetLedStatus.addActionListener(e -> {
            try {
                engine.getConfigurableLedStatus(recLedStatus -> System.out.println("Configurable led status: " + recLedStatus));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonClearLogs.addActionListener(e -> textAreaLogs.setText(null));

        buttonSetFilter.addActionListener(e -> {
            try {
                engine.setFilterReceivedListener(newFilterStatus -> System.out.println("New filter status: " + newFilterStatus));

                if (radioButtonExtended.isSelected()) {
                    engine.setFilter(FilterMode.EXTENDED);
                }

                if (radioButtonHeart.isSelected()) {
                    engine.setFilter(FilterMode.HEART);
                }

                if (radioButtonLung.isSelected()) {
                    engine.setFilter(FilterMode.LUNG);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonSetVolumeLevel.addActionListener(e -> {
            try {
                engine.setVolumeLevelReceivedListener(volumeLevel -> System.out.println("New volume level: " + volumeLevel));
                engine.setVolumeLevel(Integer.parseInt(textFieldVolumeLevel.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonSetLed.addActionListener(e -> {
            try {
                if (radioButtonOn.isSelected()) {
                    engine.setConfigurableLedStatus(true,
                            recLedStatus -> System.out.println("New recording led status: " + recLedStatus));
                }
                if (radioButtonOff.isSelected()) {
                    engine.setConfigurableLedStatus(false,
                            recLedStatus -> System.out.println("New recording led status: " + recLedStatus));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        ButtonGroup buttonGroupFilters = new ButtonGroup();
        buttonGroupFilters.add(radioButtonExtended);
        buttonGroupFilters.add(radioButtonHeart);
        buttonGroupFilters.add(radioButtonLung);

        ButtonGroup buttonGroupLedStatus = new ButtonGroup();
        buttonGroupLedStatus.add(radioButtonOn);
        buttonGroupLedStatus.add(radioButtonOff);

        ButtonGroup buttonGroupSecurityType = new ButtonGroup();
        buttonGroupSecurityType.add(radioButtonOpen);
        buttonGroupSecurityType.add(radioButtonWep);
        buttonGroupSecurityType.add(radioButtonWpa);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("eKuore Pro For Telemedicine");
        frame.setContentPane(new Tester().mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
